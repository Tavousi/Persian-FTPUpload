<?php include('header.php'); ?>
<title>FTP - جستجو</title>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">

$(function() {

    $(".uibutton").click(function() {
        // getting the value that user typed
        var searchString    = $("#search_box").val();
        // forming the queryString
        var data            = 'search='+ searchString;
        
        // if searchString is not empty
        if(searchString) {
            // ajax call
            $.ajax({
                type: "POST",
                url: "inc/search.php",
                data: data,
                beforeSend: function(html) { // this happens before actual call
                    $("#results").html(''); 
                    $("#searchresults").show();
                    $(".word").html(searchString);
               },
               success: function(html){ // this happens after we get results
                    $("#results").show();
                    $("#results").append(html);
              }
            });    
        }
        return false;
    });
});
</script>
<div style="line-height:22px;">
<div>نــام اســکریـپـت را بـه فــارسـی بـنـویـسیـد : </div>
<hr style="color:#c00;background-color:#ccc;height:1px;border:none; width:800px;"/>
<form method="post" action="do_search.php">
    <input type="text" name="search" id="search_box" class='search_box' style="width:220px;height:22px;float:right;font:8pt tahoma;"/>
    <input type="submit" value="جستجو!" class="uibutton" style="font:8pt tahoma;float:right;height:28px;margin-right:15px;"/>
</form>

<div style="clear:both;"></div>
<ul id="results" class="update">
</ul>

</div>
</div>
</div>
<?php include('footer.php'); ?>